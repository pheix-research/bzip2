#!/usr/bin/env raku

use Compress::Bzip2;

my $blockchain = '0x425a68363141592653592c4b15590000169fb140e70100002250003fefdfa00020020010003000ac4354f4011a064c8d06801864604d30264313460354fd14d943d0a0d034309a145dba653e28236d3dd2af14c5f096ed6250562cea3c24f935540d399309186aec546e49d7b68a8a12706e1cf691da03de81b6c127be190b9117e25ef3bdc20a8058747c8156359a2916e7229cb4366b094de2e8120c18da10052564254184ded3d695c690e0e55b16b0645916ac78b857d4151807902302edec60242af1d4673639d7821e563ef91f8bb9229c284816258aac80';

my $strbuf = Buf[uint8].new('My interests are in the field of real-time systems programming, development modeling software for embedded systems, test automation and software verification. I\'m a Raku (Perl6) ♥ enthusiast as well, work on module porting and contributing.'.encode);
$strbuf.bytes.say;

my $compressed = compressToBlob($strbuf);
$compressed.bytes.say;
buf2hex($compressed).say;

decompressToBlob($compressed).decode.say;
decompressToBlob(hex2buf($blockchain)).decode.say;

my $fh = 'data/datasets/set_08/utf8demo.txt'.IO.open;

my $blob = $fh.read;
$blob.bytes.say;

$compressed = compressToBlob($blob);
$compressed.bytes.say;

$fh.close;

my $fh2 = 'test.txt'.IO.open(:w, :bin);

$fh2.write($blob);
$fh2.close;

sub buf2hex(Buf[uint8] $buffer) returns Str {
    my @reencoded;
    for $buffer.List -> $data {
        my $hex = $data.base(16);
        @reencoded.push( ( $data < 16 ) ?? '0' ~ $hex !! $hex );
    }
    '0x' ~ @reencoded.join(q{});
}

sub hex2buf(Str $hex) returns Buf[uint8] {
    my @reencoded;
    my @codes = ( $hex ~~ m:g/../ ).map({ :16($_.Str) if $_ ne '0x' });
    Buf[uint8].new(@codes);
}
