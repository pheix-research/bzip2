unit class Compress::Bzip2vsLZW;

use Compress::Bzip2;
use LZW::Revolunet;

has $.dspath = './data/datasets';
has $.frlen  = 4096;
has $.lzw    = LZW::Revolunet.new(:dictsize(16384));

method get_datasets returns List {
    my @files;
    my $dir  = $!dspath;
    my @todo = $dir.IO;
    while @todo {
        for @todo.pop.dir -> $path {
            @files.push($path.Str) if $path.f;
            @todo.push: $path if $path.d;
        }
    }
    @files
}

method dataset2frames(Buf[uint8] :$dataset!) returns List {
    my @frames;
    if $dataset.bytes > 0 {
        my $ff  = ($dataset.bytes / $!frlen).ceiling;
        my $hf  = $dataset.bytes % $!frlen;
        for ^$ff -> $finx {
            if $finx < $ff {
                @frames.push($dataset.subbuf($finx * $!frlen, $!frlen));
            } else {
                @frames.push($dataset.subbuf($finx * $!frlen, $hf));
            }
        }
    }
    @frames;
}

method dataset2charframes(Str :$dataset) returns List {
    my @frames;
    if $dataset ne q{} {
        my $ff  = ($dataset.chars / $!frlen).ceiling;
        my $hf  = $dataset.chars % $!frlen;
        for ^$ff -> $finx {
            if $finx < $ff {
                @frames.push($dataset.substr($finx * $!frlen, $!frlen));
            } else {
                @frames.push($dataset.substr($finx * $!frlen, $hf));
            }
        }
    }
    @frames;
}

method frames2dataset(:@frames!) returns Buf[uint8] {
    my Buf[uint8] $dataset .= new();
    if @frames.elems > 0 {
        for @frames -> $frame {
            $dataset.push($frame);
        }
    }
    $dataset;
}

method charframes2dataset(:@frames!) returns Str {
    my Str $dataset;
    if @frames.elems > 0 {
        for @frames -> $frame {
            $dataset ~= $frame;
        }
    }
    $dataset;
}

method bzip2frames(:@frames!, Int :$ratio! is rw) returns List {
    my @frames_cmp;
    my UInt $init_size = 0;
    my UInt $cmp_size  = 0;
    if @frames.elems > 0 {
        for @frames -> $frame {
            $init_size += $frame.bytes;

            my $compressed = compressToBlob($frame);
            $cmp_size += $compressed.bytes;

            @frames_cmp.push($compressed);
        }
    }
    $ratio = $init_size/$cmp_size;
    @frames_cmp;
}

method lzw2frames(:@frames!, Int :$ratio! is rw) returns List {
    my @frames_cmp;
    my UInt $init_size = 0;
    my UInt $cmp_size  = 0;
    if @frames.elems > 0 {
        for @frames -> $frame {
            $init_size += self.getstrbytes(:s($frame));

            my Str $compressed = $!lzw.compress(:s($!lzw.encode_utf8(:s($frame))));
            $cmp_size += self.getstrbytes(:s($compressed));

            @frames_cmp.push($compressed);
        }
    }
    $ratio = $init_size/$cmp_size;
    @frames_cmp;
}

method bzipframes2dataset(:@frames!) returns Buf[uint8] {
    my Buf[uint8] $dataset .= new();
    if @frames.elems > 0 {
        for @frames -> $frame {
            $dataset.push(decompressToBlob($frame));
        }
    }
    $dataset;
}

method lzwcharframes2dataset(:@frames!) returns Str {
    my Str $dataset;
    if @frames.elems > 0 {
        for @frames -> $s {
            $dataset ~= $!lzw.decode_utf8(:s($!lzw.decompress(:$s)));
        }
    }
    $dataset;
}

method getstrbytes(Str :$s) returns UInt {
    $!lzw.get_bytes(:$s);
}
