<script src="js/camera.js"> </script>
<script>
		jQuery(function(){
			jQuery('#camera_wrap').camera({
				alignmen: 'topCenter',
				height: '44.827586206896551724137931034483%',
				minHeight: '400px',
				loader : false,
				navigation: true,
				fx: 'simpleFade',
				navigationHover:false,
				thumbnails: false,
				playPause: false,
				pagination:false,
			});
		});
</script>
											<div class="slider-block">
												<div class="fluid_container_wrap">
													<div class="fluid_container">
														<div class="camera_wrap camera_orange_skin"
															id="camera_wrap">
															<div data-src="images/USER/slides/slide1.jpg">
																<div class="camera_caption fadeFromLeft">
																	<div class="lof_camera_title">
																		<h3>
																			SONY VAIO <br> Absolute power. Beyond compare.
																		</h3>
																	</div>
																	<p>Компания Sony подтвердила слухи о продаже
																		компьютерного бизнеса под брендом VAIO.</p>
																	<a href="#" class="slider_button">Подробнее »</a>
																</div>
															</div>
															<div data-src="images/USER/slides/slide2.jpg">
																<div class="camera_caption fadeFromLeft">
																	<div class="lof_camera_title">
																		<h3>
																			SONY VAIO <br> What do you want your PC to do?
																		</h3>
																	</div>
																	<p>Компания Sony подтвердила слухи о продаже
																		компьютерного бизнеса под брендом VAIO.</p>
																	<a href="#" class="slider_button">Подробнее »</a>
																</div>
															</div>
															<div data-src="images/USER/slides/slide3.jpg">
																<div class="camera_caption fadeFromLeft">
																	<div class="lof_camera_title">
																		<h3>
																			APPLE IPHONE 6<br> Bigger than bigger
																		</h3>
																	</div>
																	<p>iPhone 6 с гладким цельным корпусом, самым
																		совершенным из наших дисплеев и множеством других
																		улучшений стал больше во всех смыслах.</p>
																	<a href="#" class="slider_button">Подробнее »</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="top-banners-container">
												<div class="row">
													<div class="clear"></div>
												</div>
											</div>
											
											<div class="category-title">
												<h1>Новинки каталога</h1>
											</div>
											<div class="new-product-carousel">
												<ul id="new-carousel" class="products-grid row">
													<li class="item first col-xs-12 col-sm-4 animated zoomIn"
														itemscope itemtype="http://schema.org/product"
														style="animation-delay: s; -webkit-animation-delay: s;">
														<div class="product-wrapper">
															<div class="product-image-container">
																<a href="shopcatalog_1327464_000021141472.html" title="" class="product-image">
																	<img src="images/USER/sliderdata/horiz/1/pix01.png"
																	width="210" alt="" />
																</a>
																<ul class="product-thumbs">
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/1/pix02.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/1/pix02.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/1/pix04.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/1/pix04.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/1/pix01.png">
																			<img
																			src="images/USER/sliderdata/horiz/1/pix01.png"
																			alt="" width="" />
																	</a></li>
																</ul>
															</div>
															<div class="product-shop">
																<div class="price-box" itemprop="offers" itemscope
																	itemtype="http://schema.org/Offer">
																	<span class="regular-price" itemprop="price"
																		id="product-price-20"> <span class="price">31168.74
																			<span class="rur">i</span>
																	</span>
																	</span>
																	<div class="clear"></div>
																</div>
																<h2 class="product-name">
																	<a href="#" title="" itemprop="name">Apple iPad Mini 3</a>
																</h2>
																<div class="desc_grid" itemprop="description">Третье поколение самого удачного компактного планшета за всю историю производства этого типа устройств. Как 10-дюймовый «Айпад», Apple iPad mini задал тон, вектор развития рынка.</div>
																<div class="actions">
																	<button type="button" title="Купить!"
																		class="button btn-cart" onclick="top.location.href='/user.pl?action=addtocart&amp;shopid=000021141472&amp;item=001174332997';">
																		<span><i class="flaticon-shopping232"></i><span>Купить!</span></span>
																	</button>
																</div>
															</div>
														</div>
													</li>
													<li class="item col-xs-12 col-sm-4 animated zoomIn"
														itemscope itemtype="http://schema.org/product"
														style="animation-delay: 0.1s; -webkit-animation-delay: 0.1s;">
														<div class="product-wrapper">
															<div class="product-image-container">
																<a href="shopcatalog_1369981_001925641720.html" title="" class="product-image">
																	<img src="images/USER/sliderdata/horiz/2/pix02.jpg"
																	width="210" alt="" />
																</a>
																<ul class="product-thumbs">
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/2/pix03.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/2/pix03.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/2/pix01.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/2/pix01.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/2/pix02.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/2/pix02.jpg"
																			alt="" width="" />
																	</a></li>
																</ul>
															</div>
															<div class="product-shop">
																<div class="price-box" itemprop="offers" itemscope
																	itemtype="http://schema.org/Offer">
																	<span class="regular-price" itemprop="price"
																		id="product-price-14"> <span class="price">93077.18
																			<span class="rur">i</span>
																	</span>
																	</span>
																	<div class="clear"></div>
																</div>
																<h2 class="product-name">
																	<a href="#" title=" Lithium-Ion Cordless Combo Kit"
																		itemprop="name"> MSI GE72 Apache Pro</a>
																</h2>
																<div class="desc_grid" itemprop="description">Ноутбук 72 6QF-012RU от компании MSI на базе процессора Intel Core i7-6700HQ 2.6 ГГц 6 МБ комплектуется оперативной памятью типа DDR3 общим размером 8 ГБ.</div>
																<div class="actions">
																	<button type="button" title="Купить!"
																		class="button btn-cart" onclick="top.location.href='/user.pl?action=addtocart&amp;shopid=001925641720&amp;item=000812096909';">
																		<span><i class="flaticon-shopping232"></i><span>Купить!</span></span>
																	</button>
																</div>
															</div>
														</div>
													</li>
													<li class="item col-xs-12 col-sm-4 animated zoomIn"
														itemscope itemtype="http://schema.org/product"
														style="animation-delay: 0.2s; -webkit-animation-delay: 0.2s;">
														<div class="product-wrapper">
															<div class="product-image-container">
																<a href="shopcatalog_1394422_002240324287.html" title="" class="product-image">
																	<img src="images/USER/sliderdata/horiz/3/pix01.png"
																	width="210"
																	alt="Samsung Galaxy S6" />
																</a>
																<ul class="product-thumbs">
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/3/pix02.png">
																			<img
																			src="images/USER/sliderdata/horiz/3/pix02.png"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/3/pix03.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/3/pix03.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/3/pix01.png">
																			<img
																			src="images/USER/sliderdata/horiz/3/pix01.png"
																			alt="" width="" />
																	</a></li>
																</ul>
															</div>
															<div class="product-shop">
																<div class="price-box" itemprop="offers" itemscope
																	itemtype="http://schema.org/Offer">
																	<span class="regular-price" itemprop="price"
																		id="product-price-10"> <span class="price">41621.51
																			<span class="rur">i</span>
																	</span>
																	</span>
																	<div class="clear"></div>
																</div>
																<h2 class="product-name">
																	<a href="#" title="" itemprop="name">Samsung Galaxy
																		S7</a>
																</h2>
																<div class="desc_grid" itemprop="description">Изготовленный
																	из металла и стекла, смартфон Samsung Galaxy S7 edge
																	сочетает в себе стильный и при этом продуманный дизайн
																	с мощными функциями.</div>
																<div class="actions">
																	<button type="button" title="Купить!"
																		class="button btn-cart" onclick="top.location.href='/user.pl?action=addtocart&amp;shopid=002240324287&amp;item=000685866462';">
																		<span><i class="flaticon-shopping232"></i><span>Купить!</span></span>
																	</button>
																</div>
															</div>
														</div>
													</li>
													<li class="item col-xs-12 col-sm-4 animated zoomIn"
														itemscope itemtype="http://schema.org/product"
														style="animation-delay: 0.3s; -webkit-animation-delay: 0.3s;">
														<div class="product-wrapper">
															<div class="product-image-container">
																<a href="shopcatalog_1389329_000676306656.html" title="" class="product-image">
																	<img src="images/USER/sliderdata/horiz/4/pix01.jpg"
																	width="210" alt="Asus Vivo AiO" />
																</a>
																<ul class="product-thumbs">
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/4/pix02.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/4/pix02.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/4/pix03.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/4/pix03.jpg"
																			alt="" width="" />
																	</a></li>
																	<li class="product-thumb"><a
																		href="images/USER/sliderdata/horiz/4/pix01.jpg">
																			<img
																			src="images/USER/sliderdata/horiz/4/pix01.jpg"
																			alt="" width="" />
																	</a></li>
																</ul>
															</div>
															<div class="product-shop">
																<div class="price-box" itemprop="offers" itemscope
																	itemtype="http://schema.org/Offer">
																	<span class="regular-price" itemprop="price"
																		id="product-price-6"> <span class="price">51436.65
																			<span class="rur">i</span>
																	</span>
																	</span>
																	<div class="clear"></div>
																</div>
																<h2 class="product-name">
																	<a href="#" title="Asus Vivo AiO V230IC"
																		itemprop="name">Asus Vivo AiO V230IC</a>
																</h2>
																<div class="desc_grid" itemprop="description">В тонком и компактном корпусе моноблоков Vivo AiO разместились все компоненты современного компьютера – дисплей, процессор, видеокарта, память, диск и многое другое.</div>
																<div class="actions">
																	<button type="button" title="Купить!"
																		class="button btn-cart" onclick="top.location.href='/user.pl?action=addtocart&amp;shopid=000676306656&amp;item=003400255534';">
																		<span><i class="flaticon-shopping232"></i><span>Купить!</span></span>
																	</button>
																</div>
															</div>
														</div>
													</li>

												</ul>
											</div>
<script>
    jQuery("#new-carousel").flexisel({
        visibleItems: 3,
        animationSpeed: 300,
        autoPlay: true,
        autoPlaySpeed: 5000,
        pauseOnHover: true,
        touch: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:380,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
    });
</script>
