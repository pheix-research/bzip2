<p>Получить бесплатную консультацию квалифицированных специалистов в сфере озеленения и посадочного материала Вы можете при обращении в нашу компанию.</p>

<p>Наша компания оказывает услуги по <strong>ДОСТАВКЕ</strong>. Рассчитать стоимость Вы можете, обратившись по нашим контактным телефонам.<br />
<br />
О Вашем визите желательно предупредить заранее.<br />
<br />
Время работы Садового центра с 1 ноября до 31 декабря 2018 г.:</p>

<p>Пн-Сб: с 9-00 до 15-00<br />
Вс: ВЫХОДНОЙ</p>

<hr />
<p><strong>Юридический адрес:</strong> 249910, Калужская область, г. Юхнов ул. Ленина 68</p>

<p><strong>Адрес офиса:</strong> 249910, Калужская область, г. Юхнов ул. Ленина 68</p>

<p><strong>Адрес склада:</strong> 249910, Калужская область, г. Юхнов ул. Ленина 68</p>

<p><strong>Контактные телефоны:</strong></p>

<p><img src="USER/images/spacer.gif" style="height:1px; width:30px" />&nbsp; <img src="USER/images/admin/cms/ingigs3.gif" style="height:7px; width:7px" />&nbsp;+7(915)207-20-44</p>

<p><img src="USER/images/spacer.gif" style="height:1px; width:30px" />&nbsp; <img src="USER/images/admin/cms/ingigs3.gif" style="height:7px; width:7px" />&nbsp;+7(495)784-98-26</p>

<p><img src="USER/images/spacer.gif" style="height:1px; width:30px" />&nbsp; <img src="USER/images/admin/cms/ingigs3.gif" style="height:7px; width:7px" />&nbsp;+7(48436)2-51-49</p>

<p><strong>Контактные E-mail адреса:</strong></p>

<p><img src="USER/images/spacer.gif" style="height:1px; width:30px" />&nbsp; <img src="USER/images/admin/cms/ingigs3.gif" style="height:7px; width:7px" />&nbsp; <a href="mailto:serg@greenposadki.ru">serg@greenposadki.ru</a></p>

<p><img src="USER/images/spacer.gif" style="height:1px; width:30px" />&nbsp; <img src="USER/images/admin/cms/ingigs3.gif" style="height:7px; width:7px" />&nbsp; <a href="mailto:mariya@greenposadki.ru">mariya@greenposadki.ru</a></p>

<hr />
<p>%WEBLOG_INSERT_FILE(conf/snippets/contacts-map.txt)%</p>
