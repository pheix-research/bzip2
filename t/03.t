use Test;
use Compress::Bzip2vsLZW;
use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant transaction       = @*ARGS[0] // %*ENV<TXHASH> // ('0x' ~ '0' x 64);
constant transaction_bytes = @*ARGS[1] // %*ENV<TXHASH> // ('0x' ~ '0' x 64);
constant skipwait          = @*ARGS[2] ?? True !! False;

constant privatekey        = 'a5b710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8';

constant smart_contact_abi       = './data/eth/v0.5.x/PheixDatabase.abi'.IO.slurp;
constant smart_contact_abi_bytes = './data/eth/v0.6.x/PheixDatabase.abi'.IO.slurp;

my $secp256k1  = Bitcoin::Core::Secp256k1.new;
my $compressor = Compress::Bzip2vsLZW.new(:frlen(4096));

my $neth = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8541'),
    :unlockpwd('node1'),
    :tx_wait_sec(5)
);

my $publickey = $secp256k1.create_public_key(:privkey(privatekey));

plan 14;

ok $neth, 'Ethereum object is ready';

if transaction eq  ('0x' ~ '0' x 64) || transaction_bytes eq  ('0x' ~ '0' x 64) {
    skip-rest('no smart contract trx addrs given');

    diag(sprintf("Usage: raku -I\$HOME/git/raku-bitcoin-core-secp256k1/lib/ -Ilib ./t/03.t <v0.5.x_addr> <v0.6.x_addr> [--skip-storing]"));
}
elsif $neth.node_ping<retcode> == -1 {
    skip-rest('ethereum node is down');
}
else {
    my $smart_contract_addr       = $neth.retrieve_contract(transaction);
    my $smart_contract_addr_bytes = $neth.retrieve_contract(transaction_bytes);

    if $smart_contract_addr ~~ m:i/^ 0x<xdigit>**40 $/ {
        my @minedhashes;
        my @hashes;

        my @tables_to_create = [
            {
                name => 'sub_test_1_table',
                sc   => $smart_contract_addr,
                abi  => smart_contact_abi
            },
            {
                name => 'sub_test_2_table',
                sc   => $smart_contract_addr,
                abi  => smart_contact_abi
            },
            {
                name => 'sub_test_3_table',
                sc   => $smart_contract_addr,
                abi  => smart_contact_abi
            },
            {
                name => 'sub_test_4_table',
                sc   => $smart_contract_addr_bytes,
                abi  => smart_contact_abi_bytes
            },
        ];

        # my $textdata = slurp $compressor.get_datasets.grep({$_ ~~ /'set_07/1377087467.txt'/})[0];
        my $textdata   = slurp $compressor.get_datasets.grep({$_ ~~ /'1483897551_cleared.txt'/})[0];
        my @framesorig = $compressor.dataset2charframes(:dataset($textdata));

        ok @framesorig.elems, sprintf("storing %d dataframes", @framesorig.elems);

        for @tables_to_create -> $t {
            $neth.contract_id = $t<sc>;
            $neth.abi         = $t<abi>;

            my %callhash = $neth.contract_method_call('tableExists', {tabname => $t<name>});

            if ((%callhash) && (%callhash<success>:exists)) {
                if %callhash<success> ~~ m:i/ true / || %callhash<success> == 1 {
                    skip sprintf("table %s exists", $t<name>), 1;

                    next;
                }
            }

            @hashes.push(
                $neth.sendTransaction(
                    :scid($neth.contract_id),
                    :fname('newTable'),
                    :fparams({
                        tabname => $t<name>,
                        fields  => '# id;data;compression;signature',
                        comp    => False
                    }),
                    :gas(3_000_000),
                )
            );

            ok @hashes.tail ~~ m:i/^ 0x<xdigit>**64 $/, sprintf("transaction to create table %s (%s) is committed", $t<name>, @hashes.tail);
        }

        if skipwait {
            skip 'forced non-wait mode', 3;
        }
        else {
            @minedhashes = $neth.wait_for_transaction(:hashes(@hashes), :iters(1_000_000));

            for @hashes -> $hash {
                is @minedhashes.grep({$_<transactionHash> eq $hash}).elems, 1, sprintf("transaction %s is mined", $hash);
            }
        }

        subtest {
            my @hashes;
            my $tdata = @tables_to_create[3];

            $neth.contract_id = $tdata<sc>;
            $neth.abi         = $tdata<abi>;

            ok $neth.personal_unlockAccount, 'account is unlocked';

            my $start = now;

            for @framesorig.kv -> $index, $dataframe {
                next unless $dataframe && $dataframe ne q{};

                my $ratio            = 0;
                my $dataframe_buf    = Buf[uint8].new($dataframe.encode);
                my ($dataframe_comp) = $compressor.bzip2frames(:frames([$dataframe_buf]), :ratio($ratio));

                @hashes.push(
                    $neth.sendTransaction(
                        :scid($neth.contract_id),
                        :fname('insert'),
                        :fparams({
                            tabname   => $tdata<name>,
                            rowdata   => $dataframe_comp,
                            id        => $index + 1,
                            comp      => False,
                            signature => signmessage(:message($dataframe)),
                        }),
                        :gas(6_000_000),
                    )
                );

                diag(sprintf("trx %s, dataframe_buf bytes %d, dataframe_comp bytes %d, ratio %.02f", @hashes.tail, $dataframe_buf.bytes, $dataframe_comp.bytes, $ratio));
            }

            if skipwait {
                skip 'forced non-wait mode', 1;
            }
            else {
                @minedhashes = $neth.wait_for_transaction(:hashes(@hashes), :iters(1_000_000));

                is @minedhashes.elems, @hashes.elems, 'dataframes are stored on blockchain';
            }

            diag(sprintf("%s compressed and locally signed dataframes are processed in %f seconds", @framesorig.elems, now - $start));

            if skipwait {
                skip 'forced non-wait mode', 1;
            }
            else {
                is get_compressed_dataframes_to_textdata(:t($tdata<name>)), $textdata, 'stored data is validated';
            }
        }, 'store compressed and locally signed dataframes';

        subtest {
            my @hashes;

            my $tdata = @tables_to_create[0];

            $neth.contract_id = $tdata<sc>;
            $neth.abi         = $tdata<abi>;

            ok $neth.personal_unlockAccount, 'account is unlocked';

            my $start = now;

            for @framesorig.kv -> $index, $dataframe {
                next unless $dataframe && $dataframe ne q{};

                @hashes.push(
                    $neth.sendTransaction(
                        :scid($neth.contract_id),
                        :fname('insert'),
                        :fparams({
                            tabname   => $tdata<name>,
                            rowdata   => $dataframe,
                            id        => $index + 1,
                            comp      => False,
                            signature => q{},
                        }),
                        :gas(6_000_000),
                    )
                );
            }

            if skipwait {
                skip 'forced non-wait mode', 1;
            }
            else {
                @minedhashes = $neth.wait_for_transaction(:hashes(@hashes), :iters(1_000_000));

                is @minedhashes.elems, @hashes.elems, 'dataframes are stored on blockchain';
            }

            diag(sprintf("%s uncompressed and unsigned dataframes are processed in %f seconds", @framesorig.elems, now - $start));
        }, 'store uncompressed and unsigned dataframes';

        subtest {
            my @hashes;

            my $tdata = @tables_to_create[1];

            $neth.contract_id = $tdata<sc>;
            $neth.abi         = $tdata<abi>;


            ok $neth.personal_unlockAccount, 'account is unlocked';

            my $start = now;

            for @framesorig.kv -> $index, $dataframe {
                next unless $dataframe && $dataframe ne q{};

                @hashes.push(
                    $neth.sendTransaction(
                        :scid($neth.contract_id),
                        :fname('insert'),
                        :fparams({
                            tabname   => $tdata<name>,
                            rowdata   => $dataframe,
                            id        => $index + 1,
                            comp      => False,
                            signature => $neth.personal_sign(:message($neth.string2hex($dataframe))),
                        }),
                        :gas(6_000_000),
                    )
                );
            }

            if skipwait {
                skip 'forced non-wait mode', 1;
            }
            else {
                @minedhashes = $neth.wait_for_transaction(:hashes(@hashes), :iters(1_000_000));

                is @minedhashes.elems, @hashes.elems, 'dataframes are stored on blockchain';
            }

            diag(sprintf("%s uncompressed and remotely signed dataframes are processed in %f seconds", @framesorig.elems, now - $start));
        }, 'store uncompressed and remotely signed dataframes';

        subtest {
            my @hashes;

            my $tdata = @tables_to_create[2];

            $neth.contract_id = $tdata<sc>;
            $neth.abi         = $tdata<abi>;


            ok $neth.personal_unlockAccount, 'account is unlocked';

            my $start = now;

            for @framesorig.kv -> $index, $dataframe {
                next unless $dataframe && $dataframe ne q{};

                signmessage(:message($dataframe));

                @hashes.push(
                    $neth.sendTransaction(
                        :scid($neth.contract_id),
                        :fname('insert'),
                        :fparams({
                            tabname   => $tdata<name>,
                            rowdata   => $dataframe,
                            id        => $index + 1,
                            comp      => False,
                            signature => signmessage(:message($dataframe)),
                        }),
                        :gas(6_000_000),
                    )
                );
            }

            if skipwait {
                skip 'forced non-wait mode', 1;
            }
            else {
                @minedhashes = $neth.wait_for_transaction(:hashes(@hashes), :iters(1_000_000));

                is @minedhashes.elems, @hashes.elems, 'dataframes are stored on blockchain';
            }

            diag(sprintf("%s uncompressed and locally signed dataframes are processed in %f seconds", @framesorig.elems, now - $start));
        }, 'store uncompressed and locally signed dataframes';
    }
    else {
        skip-rest('invalid smart contract address');
    }
}

done-testing;

sub signmessage(Str :$message!) {
    return q{} unless $message ne q{};

    my $messtosign = Node::Ethereum::Keccak256::Native.new.keccak256(:msg($message));

    (my $msg = $neth.buf2hex($messtosign)) ~~ s/^'0x'//;

    my $signature  = $secp256k1.ecdsa_sign(:privkey(privatekey), :msg($msg), :recover(True));
    my $converted = $secp256k1.recoverable_signature_convert(:sig($signature));

    if !$secp256k1.ecdsa_recover(:pubkey($publickey), :msg($msg), :sig($signature)) {
        X::AdHoc.new(:payload(sprintf("cannot recover signature:\n\t0x%s\n\t%s (%d bytes)\n\t%s (%s bytes)", $msg, $neth.buf2hex($signature), $signature.bytes, $neth.buf2hex($converted), $converted.bytes))).throw;
    }

    return $neth.buf2hex($signature);
}

sub get_compressed_dataframes_to_textdata(Str :$t!) returns Str {
    my %callhash = $neth.contract_method_call('countRows', {tabname => $t});

    if (%callhash && (%callhash<count>:exists) && %callhash<count> > 0) {
        my @data;

        for ^%callhash<count> {
            my $index = $_;

            %callhash = $neth.contract_method_call('getDataByIndex', {tabname => $t, index => $index});

            if (%callhash && (%callhash<data>:exists) && (%callhash<signature>:exists)) {
                my $compressed_data = %callhash<data>;

                %callhash<signature> ~~ s/^'0x'//;

                my $decompressed_buf = $compressor.bzipframes2dataset(:frames([$compressed_data]));
                my $dataframe        = $decompressed_buf.decode;
                my $signature        = buf8.new((%callhash<signature> ~~ m:g/../).map({ :16($_.Str)}));
                my $messtosign       = Node::Ethereum::Keccak256::Native.new.keccak256(:msg($dataframe));

                (my $msg = $neth.buf2hex($messtosign)) ~~ s/^'0x'//;

                if !$secp256k1.ecdsa_recover(:pubkey($publickey), :msg($msg), :sig($signature)) {
                    X::AdHoc.new(:payload(sprintf("cannot recover signature:\n\t0x%s\n\t%s (%d bytes)", $msg, $neth.buf2hex($signature), $signature.bytes))).throw;
                }
                else {
                    # sprintf("valid data at %s record %d", $t, $index).say;

                    @data.push($dataframe);
                }
            }
        }

        return @data.join.Str if @data.elems;
    }

    return q{};
}
