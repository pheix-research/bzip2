use Test;
use Compress::Bzip2vsLZW;
use Text::Table::Simple;
use OpenSSL::CryptTools;
use LZW::Revolunet;

plan 4;

subtest {
    my $ratio = 0;
    my $obj = Compress::Bzip2vsLZW.new(:frlen(32768));

    my $textdata = slurp $obj.get_datasets.grep({$_ ~~ /'1483897551_cleared.txt'/})[0];

    my $frame_original = $obj.dataset2charframes(:dataset($textdata))[0];

    diag(sprintf("chars = %s, original bytes = %d", $frame_original.chars, $frame_original.encode.bytes));

    ok $frame_original.encode.bytes > 0, sprintf("got %d bytes dataset", $frame_original.encode.bytes);

    my $frame_compress = $obj.bzip2frames(:frames([[Buf[uint8].new($frame_original.encode)]]), :ratio($ratio))[0];

    ok $frame_original.encode.bytes > $frame_compress.bytes, 'compression ok';
    ok $ratio > 1, sprintf('Bzip2 compression ratio %.2f', $ratio);

    diag(sprintf("bzip2 compressed bytes = %d", $frame_compress.bytes));
}, 'compression by Bzip2';

subtest {
    my $ratio_bz2 = 0;
    my $ratio_lzw = 0;

    my $obj = Compress::Bzip2vsLZW.new(:frlen(32768), :lzw(LZW::Revolunet.new(:dictsize(1))));

    my $textdata = slurp $obj.get_datasets.grep({$_ ~~ /'1483897551_cleared.txt'/})[0];

    my $frame_original = $obj.dataset2charframes(:dataset($textdata))[0];

    diag(sprintf("chars = %s, original bytes = %d", $frame_original.chars, $frame_original.encode.bytes));

    my $frame_lzwcmp = $obj.lzw2frames(:frames([$frame_original]), :ratio($ratio_lzw))[0];

    diag(sprintf("lzw compression bytes = %d", $frame_lzwcmp.encode.bytes));

    ok $obj.getstrbytes(:s($frame_original)) > $frame_lzwcmp.encode.bytes, 'LZW compression ok';
    ok $ratio_lzw > 1, sprintf('LZW compression ratio %.2f', $frame_original.encode.bytes/$frame_lzwcmp.encode.bytes);

    my $frame_compress = $obj.bzip2frames(:frames([Buf[uint8].new($frame_lzwcmp.encode)]), :ratio($ratio_bz2))[0];

    ok $obj.getstrbytes(:s($frame_original)) > $frame_compress.bytes, 'compression ok';
    ok $ratio_bz2 > 1, sprintf('Bzip2 compression ratio %.2f', $ratio_bz2);

    diag(sprintf("bzip2 compressed bytes = %d", $frame_compress.bytes));
}, 'compression by LZW -> Bzip2';

subtest {
    my $obj = Compress::Bzip2vsLZW.new();

    my Buf[uint8] $bindata = slurp $obj.get_datasets[0], :bin;

    ok $bindata.bytes > 0, sprintf("got %d bytes dataset", $bindata.bytes);

    my $frame_original  = $obj.dataset2frames(:dataset($bindata))[0];
    my $frame_compress  = $obj.bzip2frames(:frames([$frame_original]), :ratio(my $ratio = 0))[0];
    my $frame_encrypted = encrypt($frame_compress, :aes256, :iv('914e6ff3-d66d-4572-9108-96244cbbb2a8'.encode.subbuf(0,16)), :key('263d4769-9abb-48ae-ad20-816dceb48917'.encode.subbuf(0,32)));

    ok $frame_original.bytes > $frame_compress.bytes, 'compression is efficient';
    ok $frame_original.bytes > $frame_encrypted.bytes, sprintf("original = %d, compressed = %d, ecrypted = %d", $frame_original.bytes, $frame_compress.bytes, $frame_encrypted.bytes)
}, 'compression by Bzip2 and encryption by OpenSSL';

subtest {
    my $obj = Compress::Bzip2vsLZW.new();

    my Buf[uint8] $bindata = slurp $obj.get_datasets[0], :bin;

    ok $bindata.bytes > 0, sprintf("got %d bytes dataset", $bindata.bytes);

    my $frame_original  = $obj.dataset2frames(:dataset($bindata))[0];
    my $frame_encrypted = encrypt($frame_original, :aes256, :iv('914e6ff3-d66d-4572-9108-96244cbbb2a8'.encode.subbuf(0,16)), :key('263d4769-9abb-48ae-ad20-816dceb48917'.encode.subbuf(0,32)));
    my $frame_compress  = $obj.bzip2frames(:frames([$frame_encrypted]), :ratio(my $ratio = 0))[0];

    ok $frame_compress.bytes > $frame_original.bytes, 'compression is not efficient';
    ok $frame_original.bytes < $frame_encrypted.bytes, sprintf("original = %d, compressed = %d, ecrypted = %d", $frame_original.bytes, $frame_compress.bytes, $frame_encrypted.bytes)
}, 'encryption by OpenSSL and compression by Bzip2';

done-testing;
