use Test;
use Compress::Bzip2vsLZW;
use Text::Table::Simple;

plan 2;

my @lzw_ratios;
my @lzw_comp_times;
my @lzw_decomp_times;
my @bzip2_ratios;
my @bzip2_comp_times;
my @bzip2_decomp_times;

my Bool $subtest1 = subtest(
    'text dataset manipulating methods (LZW)',
    sub {
        my Rat $ts_start = 0.00;
        my Rat $ts_end   = 0.00;

        my $obj = Compress::Bzip2vsLZW.new();

        my @ds  = $obj.get_datasets();
        ok @ds.elems > 0, 'got datasets';

        for @ds -> $d {
            my Str $textdata = slurp $d;
            ok $textdata.chars > 0, 'read ' ~ $textdata.chars ~ ' chars (' ~ $obj.getstrbytes(:s($textdata)) ~ ' bytes) from ' ~ $d;

            my @frames = $obj.dataset2charframes(:dataset($textdata));
            ok @frames, 'split dataset to ' ~ @frames.elems ~ ' frames';

            my $ratio      = 0;
            $ts_start      = now.Rat;
            my @cmp_frames = $obj.lzw2frames(:frames(@frames), :ratio($ratio));

            @lzw_comp_times.push(now.Rat - $ts_start);

            ok $ratio > 0, sprintf('LZW compression ratio %.2f', $ratio);

            @lzw_ratios.push($ratio);

            my Str $checkdata = $obj.charframes2dataset(:frames(@frames));
            is-deeply $checkdata, $textdata, 'original and collected (frames) text datasets are equal' or die;

            $ts_start = now.Rat;
            my Str $decompdata = $obj.lzwcharframes2dataset(:frames(@cmp_frames));

            @lzw_decomp_times.push(now.Rat - $ts_start);

            is-deeply $decompdata, $textdata, 'original and decompressed text datasets are equal' or die;
        }
    }
).Bool;

my Bool $subtest2 = subtest(
    'binary dataset manipulating methods (Bzip2)',
    sub {
        my Rat $ts_start = 0.00;
        my Rat $ts_end   = 0.00;

        my $obj = Compress::Bzip2vsLZW.new();

        my @ds  = $obj.get_datasets();
        ok @ds.elems > 0, 'got datasets';

        for @ds -> $d {
            my Buf[uint8] $bindata = slurp $d, :bin;
            ok $bindata.bytes > 0, 'read ' ~ $bindata.bytes ~ ' bytes from ' ~ $d;

            my @frames = $obj.dataset2frames(:dataset($bindata));
            ok @frames, 'split dataset to ' ~ @frames.elems ~ ' frames';

            my $ratio      = 0;
            $ts_start      = now.Rat;
            my @cmp_frames = $obj.bzip2frames(:frames(@frames), :ratio($ratio));

            @bzip2_comp_times.push(now.Rat - $ts_start);

            ok $ratio > 0, sprintf('Bzip2 compression ratio %.2f', $ratio);

            @bzip2_ratios.push($ratio);

            my Buf[uint8] $checkdata = $obj.frames2dataset(:frames(@frames));
            is-deeply $checkdata, $bindata, 'original and collected (frames) binary datasets are equal' or die;

            $ts_start = now.Rat;
            my Buf[uint8] $decompdata = $obj.bzipframes2dataset(:frames(@cmp_frames));

            @bzip2_decomp_times.push(now.Rat - $ts_start);

            is-deeply $decompdata, $bindata, 'original and decompressed binary datasets are equal' or die;
        }
    }
).Bool;

done-testing;

my @headers = ['cmp type', 'iters', 'average ratio', 'avg cmp time', 'avg decmp time'];
my @rows = [
    [
        'lzw',
        @lzw_ratios.elems,
        @lzw_ratios.sum/@lzw_ratios.elems,
        @lzw_comp_times.sum/@lzw_comp_times.elems,
        @lzw_decomp_times.sum/@lzw_decomp_times.elems
    ],
    [
        'bzip2',
        @bzip2_ratios.elems,
        @bzip2_ratios.sum/@bzip2_ratios.elems,
        @bzip2_comp_times.sum/@bzip2_comp_times.elems,
        @bzip2_decomp_times.sum/@bzip2_decomp_times.elems
    ]
];

my @table = lol2table(@headers, @rows);

diag($_) for @table;
